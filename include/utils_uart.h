/*******************************************************************************
C header: the risc-v calling convention use a char type for 8 bits long data,
so all MACRO use the same as input/output

-> UART access for read/write

-> UART access for read/write in to the register data, with check of the current
control/status of the device

-> Baud Rate MACROS

-> Printf function like MACROS, through UART
********************************************************************************/

#ifndef __AICA_IO_H__
#  error "<aica_io.h> should be included before <utils_uart.h> file."
#endif

#ifndef __UTILS_UART_H__
#define __UTILS_UART_H__

// *****************************************************************************
// Baud Rate MACROS
#define b1200       0
#define b2400       1
#define b4800       2
#define b9600       3
#define b19200      4
#define b38600      5
#define b57400      6
#define b115200     7

// *****************************************************************************
// UART read/write access, read any of it's register, with a type char
#define	uart_check(__io_addr__) _IO_RW_BYTE_(__io_addr__)

#define	uart_get(__io_addr__ , __char_data__)   __char_data__=_IO_RW_BYTE_(__io_addr__)

#define uart_set(__io_addr__ , __char_data__)   _IO_RW_BYTE_(__io_addr__)=__char_data__

// *****************************************************************************
// UART read data register, it uses a loop to check the control/status register
#define uart_rpoll(__char_data__)                       \
    __char_data__ = 0x00;                               \
    while ( (uart_check(UART_CTRL) & 0x01 ) == 0x00){}  \
    uart_get(UART_DATA , __char_data__);

// *****************************************************************************
// UART write data register, it uses a loop to check the control/status register
#define uart_wpoll(__char_data__)                       \
    while ( (uart_check(UART_CTRL) & 0x02 ) == 0x00){}  \
    uart_set(UART_DATA , __char_data__ );

// *****************************************************************************
// UART enable write/read interrupt (doesn't use a mask)
#define uart_rintr_en() \
    uart_set(UART_CTRL , 0x10);

#define uart_wintr_en() \
    uart_set(UART_CTRL , 0x20);

#define uart_rwintr_en() \
    uart_set(UART_CTRL , 0x70);

#define uart_intr_dis() \
    uart_set(UART_CTRL , 0x00);

// *****************************************************************************
// -> ascii_hex2int: Converts ASCII char that represents a HEX value to int,
// returns -1 if not a valid hex value
// -> int2ascii: Converts int value to ASCII char
#define ascii_hex2int(__ascii_data__ , __int_data__)            \
    if (__ascii_data__ >= '0' && __ascii_data__ <= '9'){        \
        __int_data__ = __ascii_data__ - '0';}                   \
    else if (__ascii_data__ >= 'A' && __ascii_data__ <= 'F'){   \
        __int_data__ = __ascii_data__ - 'A' + 10;}              \
    else if (__ascii_data__ >= 'a' && __ascii_data__ <= 'f'){   \
        __int_data__ = __ascii_data__ - 'a' + 10;}              \
    else{                                                       \
        __int_data__ = -1;}
    

#endif

#define int2ascii(__str__ , __int__);                           \
    int __rem__;                                                \
    for (int __k__ = 0 ; __k__ < sizeof(__str__) ; __k__++)     \
    {                                                           \
        if (__int__ == 0){                                      \
            __str__[__k__] = '\0';}                             \
        else{                                                   \
            __rem__ = __int__ % 10;                             \
            __int__ /= 10;                                      \
            __str__[__k__] = __rem__ + '0';                     \
        }                                                       \
    }

// *****************************************************************************
// UART printf like MACRO
// -> null terminated string
#define STDIO_UART(); \
    void printf_int_uart(int data);             \
    void printf_int_uart(int data)              \
    {                                           \
        volatile char str[20];                  \
        int	k;                                  \
                                                \
        int2ascii(str , data);                  \
                                                \
        for (k = sizeof(str)-1 ; k >= 0 ; k--)  \
        {                                       \
            if (str[k] != '\0'){                \
                uart_wpoll(str[k]);}            \
        }                                       \
    }                                           \
                                                \
    void printf_uart(const char *p, ...);       \
    void printf_uart(const char *p, ...)        \
    {                                           \
        const char* str_p;                      \
        va_list     args;                       \
        va_start    (args , p);                 \
                                                \
        for (int k = 0 ; *(p+k) != '\0' ; k++){ \
            if (*(p+k) == '%'){                 \
                k++;                            \
                if (*(p+k) == 'd'){             \
                    printf_int_uart( va_arg(args , int) );}     \
                else if (*(p+k) == 'c'){                        \
                    uart_wpoll( va_arg(args , int) );}          \
                else if (*(p+k) == 's'){                        \
                    str_p = va_arg(args , const char*);         \
                    for (int l = 0 ; *(str_p+l) != '\0' ; l++){ \
                        uart_wpoll( *(str_p+l) );}              \
                }                                               \
            }                                                   \
            else{                                               \
                uart_wpoll( *(p+k) );}                          \
        } \
    }
