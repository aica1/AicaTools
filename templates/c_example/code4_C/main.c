#include <aica_io.h>
#include <utils_uart.h>
#include <stdarg.h>

STDIO_UART();

// *****************************************************************************
int main (){
    // matrix data
    int rowA = 100;
    int colA = 100;
    int rowB = 100;
    int colB = 100;

    int sum;
    int matrixA[rowA][colA];
    int matrixB[rowB][colB];

    volatile int Mc[rowA][colA];
    // float sum;
    // float matrixA[rowA][colA];
    // float matrixB[rowB][colB];
    // volatile float Mc[rowA][colA];

    // loop ctrl and others
    int c, d, k;

    // timer
    int t1;
    int t2;
    volatile int tf;

    // get timer value
    timer0_read(t1);

    // M1 * M2
    sum = 0;
    if (colA != rowB){}
    else{
        for (c = 0; c < rowA; c++){
            for (d = 0; d < colB; d++){
                for (k = 0; k < rowB; k++){
                    sum = sum + matrixA[c][k]*matrixB[k][d];}

                Mc[c][d] = sum;
                sum = 0;
            }
        }
    }

    // get timer value
    timer0_read(t2);
    printf_uart("Marcas de tempo: tempo_final = t2 - t1\n  t1 = %d   ;   t2 = %d\n\n" , t1 , t2);

    // show time measure result
    tf = t2 - t1;
    printf_uart("Número de ciclos (100 MHz): %d\n" , tf );

    // inf loop
    while (1) {}
}
