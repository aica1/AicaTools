#include <aica_io.h>
#include <utils_uart.h>

/* ------------------------------------
	Código exemplo: acesso a uart (polling)
	headers: utils_uart.h
------------------------------------ */

// -------------------------------------------------
// -------------------------------------------------
// main code
int main ()
{

	/* MACROS do header principal, aica_io.h:
		Endereços:
			UART_CTRL:	end. do regs. de controle da UART
			UART_DATA:	end. do regs. de dados da UART
			UART_BAUD:	end. do regs. de baudrate da UART

			TIMER0_DATA:	end. do regs. do temporizador 0

		Acesso:
			_IO_RW_BYTE_(__io_addr__):	escreve/le um byte no endereço __io_addr__
			_IO_RW_HALF(__io_addr__):	escreve/le meia palavra no endereço __io_addr__
			_IO_RW_WORD(__io_addr__):	escreve/le uma palavra no endereço __io_addr__
	 */


	/* MACROS para leitura/escrita de regs. da UART:
		uart_get(__io_addr__ , __char_data__):	salva em __char_data__ o valor contido no regs.,
												dado pelo end. __io_addr__
		uart_set(__io_addr__ , __char_data__):	salva no regs. contido no end. __io_addr__, o
												valor de __char_data__

		exemplo: ajustando baudrate
			uart_set(UART_BAUD , b19200);

		possíveis valores de baudrate definidos por MACROS:
			b1200, b2400, b4800, b9600, b19200, b38600, b57400 e b115200

	 */

	/* MACROS de acesso ao regs. de dado da UART por polling: 
	
		uart_wpoll(__char_data__) -> transmite __char_data__ por meio da técnica de polling,
									possuí um laço interno de verificação do regs. de controle
		uart_rpoll(__char_data__) -> faz a leitura de __char_data__ por meio da técnica de polling,
									possuí um laço interno de verificação do regs. de controle
	*/

	/* MACROS de configuração da UART:
			uart_rintr_en();-> expande para uart_set(UART_CTRL , 0x10);
							habilita a lógica de interrupção para recepção de dados
			uart_wintr_en();-> expande para uart_set(UART_CTRL , 0x20)
							habilita a lógica de interrupção para transmissão de dados
			uart_rwintr_en(); -> expande para uart_set(UART_CTRL , 0x70);
							habilita a lógica de interrupção para recepção/transmissão de dados
							simultâneos, tem prioridade sobre uart_rintr_en e uart_wintr_en
			uart_intr_dis();-> expande para uart_set(UART_CTRL , 0x00);
							desabilita toda a lógica de interrupção
	 */

	char data;

	uart_intr_dis();
	uart_set(UART_BAUD , b115200);

	// inf loop, necessário
	while (1) {

		// faz a leitura de um dado recebido, e o devolve
		uart_rpoll(data);
		uart_wpoll(data);

	}
}
