#include <aica_io.h>
#include <utils_uart.h>
#include <csr.h>
#include <trap_handler.h>

// *****************************************************************************
int main (){
    int  count = 3;
    char nop;

    // configure uart
    uart_set(UART_BAUD , b115200);

    // inf loop
    while (1) {
        uart_rpoll(nop);
        uart_wpoll(count);
        count = count * 4;
    }
}
