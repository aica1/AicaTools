#include <aica_io.h>
#include <csr.h>
#include <trap_handler.h>

/* ------------------------------------
	Código exemplo: exceções/interrupções
	headers: csr.h e trap_handler.h
		-> csr.h contém MACRO com asm inline para
			configuração dos CSR;
		-> trap_handler.h contém MACRO para declaração
			das funções de tratamento.
------------------------------------ */

// -------------------------------------------------
// -------------------------------------------------
/* A MACRO MAIN_HANDLER contém uma função pronta para tratamento de exceções/interrupções
	com o CSR mtvec no modo DIRECT, caso não se declare, é necessário escrever manualmente
	uma função na seção main trap handler, exemplo:
			void _main_trap_() __attribute__((section("._m_trap_handler_"))) __attribute__((interrupt("machine")));
			void _main_trap() 
			{
				// code
			};
*/

MAIN_HANDLER();				// main trap handler

/* A MACRO INTR_VECT possuí como entrada o nome da função de tratamento e o número
correspondente do tipo de interrupção, para que se coloque o endereço de desvio (end.
da função de tratamento) na posição correta da tabela de vetores de interrupção.
Os tipos de exceção/interrupção estão definidos no manual privilegiado do RISC-V.
Para declaração de funções de tratamento de exceções, a MACRO é EXCP_VECT
*/
INTR_VECT(uart_intr , 11);	// intr vector for external interrupt

/* os valores disponíveis de número de interrupção e exceção são:
		EXCEÇÃO:
			0 = end. de instr. desalinhado (causado por jump/branch)
			2 = instr ilegal
			4 = end. de dado desalinhado (causado por load/store)
		INTERRUPÇÃO:
			11 = interrupção externa (unica disponível é a UART)
*/

// -------------------------------------------------
// -------------------------------------------------
// main code
int main ()
{

	/* MACROS de configuração dos CSR estão disponíveis no header csr.h,
	executam asm inline, correspondendo as instruções CSRRW, CSRRS, CSRRC,
	CSRRWI, CSRRSI e CSRRCI
		csr_rw()
		csr_r()
		csr_w()
		csr_rwi()
		csr_wi()
		csr_rs()
		csr_s()
		csr_rsi()
		csr_si()
		csr_rc()
		csr_c()
		csr_rci()
		csr_ci()

		exemplo (obs.: nem todos os campos dos CSRs estão disponíveis para escrita no hardware):
			int x;

			x = csr_rw(mstatus , 5);	// carrega para x o valor antigo de mstatus, e escreve 5 no mstatus
			x = csr_rwi(mtvec , 3);		// carrega para x o valor antigo de mtvec, e escreve 3 no mtvec

		Todas as MACRO terminadas em 'i' (prefixo) são operações immediate, e o maior valor
		possível para se utilizar é 31, que seria em:
			csr_rwi, csr_wi, csr_rsi, csr_si e csr_rci

		exemplo de leitura e escritas separadas:
			int x;

			x = csr_r(mstatus);
			csr_w(mstatus , x);
	*/

	/* MACROS de configuração CSR específicos:
			mtrap_en(); -> expande para csr_si(mstatus , 8);
							habilitação global de trap
			mtrap_dis();-> expande para csr_ci(mstatus , 8);
							desabilitação global de trap
			mexcp_en();	-> expande pra csr_si(mie , 8);
							habilitação de exceções
			mexcp_dis();-> expande para csr_ci(mie , 8);
							desabilitação de exceções
			mintr_en();	-> expande para 
							int _val_ = 0x00000800;
							csr_s(mie , _val_);
							habilitação de interrupções
			mintr_dis();-> expande para 
							int _val_ = 0x00000800;
							csr_c(mie , _val_);
							desabilitação de interrupções
			mtvec_mode(_val_); -> expande para csr_w(mtvec , _val_);
							define o modo entre DIRECT (_val_ = 0) ou
								VECTOR (_val_ = 1)

	 */
	mintr_en();						// enable external interrupts
	mtrap_en();						// enable global trap

	// inf loop, necessário
	while (1) {}
}

// -------------------------------------------------
// -------------------------------------------------
// função de tratamento
void uart_intr()
{
	// code
}
