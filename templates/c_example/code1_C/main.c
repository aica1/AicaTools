#include <aica_io.h>
#include <utils_uart.h>
#include <csr.h>
#include <trap_handler.h>

// *****************************************************************************
MAIN_HANDLER();             // main trap handler
INTR_VECT(uart_intr , 11);  // intr vector for external interrupt

// *****************************************************************************
int main (){
    // configure the UART
    uart_set(UART_BAUD , b115200);
    uart_rwintr_en();   // enable interrupts

    // configure the CSR
    mintr_en(); // enable external interrupts
    mtrap_en(); // enable global trap

    // inf loop
    while (1) {}
}

// *****************************************************************************
void uart_intr(){
    char data;

    uart_get(UART_DATA , data);
    uart_set(UART_DATA , data);
}
