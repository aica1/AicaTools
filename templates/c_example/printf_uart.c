#include <aica_io.h>
#include <utils_uart.h>
#include <stdarg.h>

STDIO_UART();

// -------------------------------------------------
// -------------------------------------------------
int main ()
{

	// matrix data
	int rowa = 3;
	int cola = 3;
	int rowb = 3;
	int colb = 3;

	//---- Integer Matrix
	int sum;
	int Ma[3][3] ={
		{0, 1, 2} ,
		{4, 5, 6} ,
		{8, 9, 10}		};

	int Mb[3][3] ={
		{12, 13, 14} ,
		{15, 16, 17} ,
		{18, 19, 20}	};

	volatile int Mc[rowa][cola];

	//---- Float Matrix
/* 	float sum;
	float Ma[3][3] ={
		{0.6, 1.5, 2.4} ,
		{4.1, 5.25, 6.3} ,
		{8.152, 9.12, 10.125}		};

	float Mb[3][3] ={
		{12.11, 13.013, 14.41} ,
		{15.33, 16.87, 17.136} ,
		{18.1316, 19.614, 20.136}	};

	volatile float Mc[rowa][cola]; */

	// loop ctrl and others
	int c, d, k;

	// timer
	int t1;
	int t2;
	volatile int tf;

	// get timer value
	timer0_read(t1);

	//-----------------------------
	// M1 * M2
	sum = 0;
	if (cola != rowb){}
	else
	{
		for (c = 0; c < rowa; c++)
		{
			for (d = 0; d < colb; d++)
			{
				for (k = 0; k < rowb; k++)
				{
					sum = sum + Ma[c][k]*Mb[k][d];
				}

				Mc[c][d] = sum;
				sum = 0;
			}
		}
	}

	// get timer value
	timer0_read(t2);
	printf_uart("%s\n  t1 = %d   ;   t2 = %d\n" , "Valores do temporizador:" , t1 , t2);

	//------------------
	// show time measure result
	tf = t2 - t1;
	printf_uart("\nNumber of clock cycles: %d\n" , tf );

	//------------------
	// show Mc result (final Matrix)
	printf_uart("\n  Matrix Mc = \n[\n");
	for (c = 0 ; c < rowa ; c++)
	{
		for (d = 0 ; d < cola ; d++)
		{
			printf_uart("%d  " ,  (int) Mc[c][d] );
		}
		printf_uart("\n");
	}
	printf_uart("]\n");

	// inf loop
	while (1)
	{
	}
}
