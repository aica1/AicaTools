# c files
CC_EXT			= c
CC_SRC_PATH		= src
CC_SRC_FILE		= $(wildcard $(CC_SRC_PATH)/*.$(CC_EXT))

# headers
CC_HEADER_PATH	= include
CC_HEADER_FILE	= $(wildcard $(CC_HEADER_PATH)/*.h)

# compilr
CC				= riscv32-unknown-elf-gcc
CC_ARCH			= -misa-spec=2.2 -march=rv32i -mabi=ilp32 -mstrict-align -mpreferred-stack-boundary=3
CC_FLAGS		= -nostartfiles -O1 -Wall -Wl,--build-i=none

CC_LINK			= -T link.ld
CC_INC_PATH		= -I$(CC_HEADER_PATH)
CC_BUILD_PATH	= build

CC_OUTPUT		= $(CC_BUILD_PATH)/main.elf
CC_FILE			= $(basename $(notdir $(CC_OUTPUT)))

# objects
CC_OBJ_PATH		= $(CC_BUILD_PATH)/obj
CC_OBJ_FILE		= $(patsubst $(CC_SRC_PATH)/%.$(CC_EXT), $(CC_OBJ_PATH)/%.o, $(CC_SRC_FILE))

# strip command
CC_STRIP		= riscv32-unknown-elf-strip
CC_STRIP_FLAGS	= --remove-section=.comment --remove-section=.riscv.attributes

# dump command
CC_DUMP			= riscv32-unknown-elf-objdump
CC_DUMP_FLAGS	= -D -Mnumeric,no-aliases
CC_DUMP_PATH	= $(CC_BUILD_PATH)/dump
CC_DUMP_FILE	= $(CC_DUMP_PATH)/$(CC_FILE).dump

# hex command
CC_HEX			= riscv32-unknown-elf-objcopy
CC_HEX_FLAGS	= -O ihex
CC_HEX_PATH		= $(CC_BUILD_PATH)/hex
CC_HEX_FILE		= $(CC_HEX_PATH)/$(CC_FILE).hex

DIR_STRUCTURE_a	= $(CC_SRC_PATH) $(CC_HEADER_PATH) $(CC_BUILD_PATH)
DIR_STRUCTURE	= $(DIR_STRUCTURE_a) $(CC_OBJ_PATH) $(CC_DUMP_PATH) $(CC_HEX_PATH)

# ******************************************************************************
# hex
$(CC_HEX_FILE): $(CC_DUMP_FILE)
	$(CC_HEX) $(CC_HEX_FLAGS) $(CC_OUTPUT) $(CC_HEX_FILE)

# dump
$(CC_DUMP_FILE): $(CC_OUTPUT)
	$(CC_DUMP) $(CC_DUMP_FLAGS) $(CC_OUTPUT) > $(CC_DUMP_FILE)

# output
$(CC_OUTPUT): $(CC_OBJ_FILE)
	$(CC) $(CC_ARCH) $(CC_FLAGS) $(CC_LINK) $(CC_OBJ_FILE) -o $(CC_OUTPUT)	&&\
	$(CC_STRIP) $(CC_STRIP_FLAGS) $(CC_OUTPUT) -o $(CC_OUTPUT)

# objects
$(CC_OBJ_PATH)/%.o: $(CC_SRC_PATH)/%.$(CC_EXT) | $(DIR_STRUCTURE)
	$(CC) $(CC_ARCH) $(CC_FLAGS) $(CC_INC_PATH) -c $^ -o $@

# directory structure
$(DIR_STRUCTURE):
	mkdir -p -v $@

# phony targets
.PHONY: force clean

force:
	make clean	&&\
	make

clean:
	rm -v -f $(CC_OUTPUT) $(CC_OBJ_PATH)/* $(CC_DUMP_PATH)/* $(CC_HEX_PATH)/*
