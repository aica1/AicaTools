#include <aica_io.h>
#include <utils_uart.h>

/* iHex Fields Define */
#define	TT_data 0   // data
#define TT_eof  1   // end of file
#define	TT_esa  2   // extended segment address
#define TT_ssa  3   // start segment address
#define TT_ela  4   // extended linear address
#define TT_sla  5   // start linear address

/* Functions with section attribute */
int bootloader_ihex() __attribute__ ((section("._bootloader_ihex_")));
int uart_read_ascii2int() __attribute__ ((section("._bootloader_ihex_")));

// *****************************************************************************
// Bootloader program to read a code through UART,
// and then store it in the RAM.
int bootloader_ihex()
{
    // Strings
    char    init_str[2] = ">\n";

    // ihex control
    int     LL      = 0;        // ihex line num of bytes
    int     AAAA_lb = 0;        // ihex line start address, lower bytes
    int     AAAA_ext= 0;        // ihex line start address, extended bytes
    int     AAAA    = 0;        // ihex line start address
    int     TT      = TT_data;  // ihex line record
    char    DD      = 0;        // ihex line data (char is a 8 bit integer in RISC-V convention)

    // others
    int     k       = 0;        // loop ctrl
    int     i       = 0;        // loop ctrl

    // Send boot msg 'init_str' and then
    // wait for the first ':' ASCII char from
    // the ihex file, and send a confirmation msg
    uart_set(UART_BAUD , b115200);

    for (k = 0 ; k < sizeof(init_str) ; k++){
        uart_wpoll(init_str[k]);}

    while (TT != TT_eof){
    /*
    IHEX FILE FORMAT => :LLAAAATTDDDDCC
        NOTE: from left to right means MSB on left

        ':'     => line start, can be ignored;
        'LL'    => from left to right, a hexadecimal value holding the num of data bytes in this line;
        'AAAA'  => from left to right, a hexadecimal value holding the start address of this line,
                    at each 1 data byte ('DD') increment by 1;
        'TT'    => line record, says the type of content in the D field:
                    00 = data;	01 = end of file;   02 = extended segment address
                    03 = start segment address;     04= extended linear address;
                    05 = start linear address.
        'DDDD'  => data field (can be a data or address, depends on the TT field):
                    data = from left to right, a hexadecimal value (at each 2 DD) holding 1 byte;
                    address = from left to right, a hexadecimal value holding an address, can be
                            4 ou 8 DD, depends on the type of address record.
        'CC'    => line checksum, used for verification (can be ignored)
    */

        // Wait for start of line (:)
        DD = '0';

        while (DD != ':') {
            uart_rpoll(DD); }

        uart_wpoll(':');

        // Read LL field (num of bytes)
        LL = 0;
        for (k = 0 ; k < 2 ; k++){
            LL = (LL << 4);
            LL = LL + uart_read_ascii2int();
        }

        // Read AAAA field (start address)
        AAAA_lb = 0;
        for (k = 0 ; k < 4 ; k++){
            AAAA_lb = (AAAA_lb << 4);
            AAAA_lb = AAAA_lb + uart_read_ascii2int();
        }

        // Read TT field (record type)
        TT = 0;
        for (k = 0 ; k < 2 ; k++){
            TT = (TT << 4);
            TT = TT + uart_read_ascii2int();
        }

        // Read DD field (data)

        // set start address
        if (TT != TT_data && TT != TT_eof)
            AAAA_ext = 0;

        AAAA = AAAA_lb + AAAA_ext;

        // read/write data
        for (k = 0 ; k < LL ; k++){
            // read
            DD = 0;
            for (i = 0 ; i < 2 ; i++){
                DD = (DD << 4);
                DD = DD + uart_read_ascii2int();
            }

            // write
            if (TT == TT_data){ // record: data type
                __asm__ __volatile__
                (
                    "sb %0,0(%1)"
                    : : "r" (DD), "r" (AAAA)
                );
                // address increment
                AAAA++;
            }
            else if (TT != TT_eof){ // record: address type
                AAAA_ext = AAAA_ext << 8;
                AAAA_ext = AAAA_ext + DD;
            }
        }

        // Read CheckSum field (ignored)
        // ignored
        DD = 0;
        while (DD != '\n'){
            uart_rpoll(DD);
            uart_wpoll(DD);
        }

        // Set the final extended address (if TT = extended address type)
        if (TT == TT_ela)       /* extended linear address */
            AAAA_ext = AAAA_ext << 16;
        else if (TT == TT_esa){ /* extended segment address */
            AAAA_ext = AAAA_ext << 4;
            AAAA_ext = AAAA_ext & 0x000FFFF0;   // clear bits [31:20]
        }
        // else if (TT == TT_sla)  /* start linear address: already done inside the for previous 'for' */
    }

    for (k = 0 ; k < sizeof(init_str) ; k++){
        uart_wpoll(init_str[k]);}

    return AAAA_ext;
}

// *****************************************************************************
// Converts ASCII Hex to int value
int uart_read_ascii2int()
{
    int  int_data;
    char ascii_data = '\n';

    // macros from <utils_uart.h>
    uart_rpoll(ascii_data);
    uart_wpoll(ascii_data);

    ascii_hex2int(ascii_data , int_data);

    return int_data;
}
