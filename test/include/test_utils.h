#ifndef TEST_UTILS_H
#define TEST_UTILS_H

// *****************************************************************************
// Verification Macro
// *****************************************************************************

// ADDR_TEST_STATUS = store a 32 bits values with the tests result, which is
//                    either the string "PASS" or "FAIL"
// ADDR_TEST_CASE   = ID of the last test executed (for debug)
// ADDR_TEST_TEMP   = for any other value
// ADDR_TEST_REGFILE= 32x32 bytes reserved to dump the register file content
// Note: these addresses used for test should start at the lower range of
//       the memory, since intend to build the address only with immediate signals
#define ADDR_TEST_STATUS  0
#define ADDR_TEST_CASE    ADDR_TEST_STATUS+4
#define ADDR_TEST_TEMP    ADDR_TEST_STATUS+8
#define ADDR_TEST_REGFILE ADDR_TEST_STATUS+12

#define ADDR_TEST_REGFILE_ID(x) ADDR_TEST_REGFILE+(4*x)

// Store "PASS" string at the result address
#define PASS                    \
test_pass:                      \
  li  x1, 0x50415353;           \
  sw  x1,ADDR_TEST_STATUS(x0);  \
loop_test_pass:                 \
  bne  x31, x0, loop_test_pass;

// Store "FAIL" string at the result address
#define FAIL                    \
test_fail:                      \
  li  x1, 0x4641494C;           \
  sw  x1, ADDR_TEST_STATUS(x0); \
loop_test_fail:                 \
  bne  x31, x0, loop_test_fail;

// Increment current test number
#define START_TEST  \
  sw   x1, ADDR_TEST_TEMP(x0);  \
  lw   x1, ADDR_TEST_CASE(x0);  \
  addi x1, x1, 1;               \
  sw   x1, ADDR_TEST_CASE(x0);  \
  lw   x1, ADDR_TEST_TEMP(x0);

#define RST_TEST_INFO \
  sw x0, ADDR_TEST_STATUS(x0);  \
  sw x0, ADDR_TEST_CASE(x0);

// dump register file content at the pre-defined memory section
#define STORE_REGFILE   \
  sw  x0,  ADDR_TEST_REGFILE_ID(0) (x0);  \
  sw  x1,  ADDR_TEST_REGFILE_ID(1) (x0);  \
  sw  x2,  ADDR_TEST_REGFILE_ID(2) (x0);  \
  sw  x3,  ADDR_TEST_REGFILE_ID(3) (x0);  \
  sw  x4,  ADDR_TEST_REGFILE_ID(4) (x0);  \
  sw  x5,  ADDR_TEST_REGFILE_ID(5) (x0);  \
  sw  x6,  ADDR_TEST_REGFILE_ID(6) (x0);  \
  sw  x7,  ADDR_TEST_REGFILE_ID(7) (x0);  \
  sw  x8,  ADDR_TEST_REGFILE_ID(8) (x0);  \
  sw  x9,  ADDR_TEST_REGFILE_ID(9) (x0);  \
  sw  x10, ADDR_TEST_REGFILE_ID(10)(x0);  \
  sw  x11, ADDR_TEST_REGFILE_ID(11)(x0);  \
  sw  x12, ADDR_TEST_REGFILE_ID(12)(x0);  \
  sw  x13, ADDR_TEST_REGFILE_ID(13)(x0);  \
  sw  x14, ADDR_TEST_REGFILE_ID(14)(x0);  \
  sw  x15, ADDR_TEST_REGFILE_ID(15)(x0);  \
  sw  x16, ADDR_TEST_REGFILE_ID(16)(x0);  \
  sw  x17, ADDR_TEST_REGFILE_ID(17)(x0);  \
  sw  x18, ADDR_TEST_REGFILE_ID(18)(x0);  \
  sw  x19, ADDR_TEST_REGFILE_ID(19)(x0);  \
  sw  x20, ADDR_TEST_REGFILE_ID(20)(x0);  \
  sw  x21, ADDR_TEST_REGFILE_ID(21)(x0);  \
  sw  x22, ADDR_TEST_REGFILE_ID(22)(x0);  \
  sw  x23, ADDR_TEST_REGFILE_ID(23)(x0);  \
  sw  x24, ADDR_TEST_REGFILE_ID(24)(x0);  \
  sw  x25, ADDR_TEST_REGFILE_ID(25)(x0);  \
  sw  x26, ADDR_TEST_REGFILE_ID(26)(x0);  \
  sw  x27, ADDR_TEST_REGFILE_ID(27)(x0);  \
  sw  x28, ADDR_TEST_REGFILE_ID(28)(x0);  \
  sw  x29, ADDR_TEST_REGFILE_ID(29)(x0);  \
  sw  x30, ADDR_TEST_REGFILE_ID(30)(x0);  \
  sw  x31, ADDR_TEST_REGFILE_ID(31)(x0);

// Fill register file with 0s
#define CLEAR_REGFILE  \
  addi  x1,  x0, 0;  \
  addi  x2,  x0, 0;  \
  addi  x3,  x0, 0;  \
  addi  x4,  x0, 0;  \
  addi  x5,  x0, 0;  \
  addi  x6,  x0, 0;  \
  addi  x7,  x0, 0;  \
  addi  x8,  x0, 0;  \
  addi  x9,  x0, 0;  \
  addi  x10, x0, 0;  \
  addi  x11, x0, 0;  \
  addi  x12, x0, 0;  \
  addi  x13, x0, 0;  \
  addi  x14, x0, 0;  \
  addi  x15, x0, 0;  \
  addi  x16, x0, 0;  \
  addi  x17, x0, 0;  \
  addi  x18, x0, 0;  \
  addi  x19, x0, 0;  \
  addi  x20, x0, 0;  \
  addi  x21, x0, 0;  \
  addi  x22, x0, 0;  \
  addi  x23, x0, 0;  \
  addi  x24, x0, 0;  \
  addi  x25, x0, 0;  \
  addi  x26, x0, 0;  \
  addi  x27, x0, 0;  \
  addi  x28, x0, 0;  \
  addi  x29, x0, 0;  \
  addi  x30, x0, 0;  \
  addi  x31, x0, 0;

#define RANDOM_REGFILE  \
  addi  x1,  x0, 1;   \
  addi  x2,  x0, 8;   \
  addi  x3,  x0, 15;  \
  addi  x4,  x0, 22;  \
  addi  x5,  x0, 29;  \
  addi  x6,  x0, 36;  \
  addi  x7,  x0, 43;  \
  addi  x8,  x0, 50;  \
  addi  x9,  x0, 57;  \
  addi  x10, x0, 64;  \
  addi  x11, x0, 71;  \
  addi  x12, x0, 78;  \
  addi  x13, x0, 85;  \
  addi  x14, x0, 92;  \
  addi  x15, x0, 99;  \
  addi  x16, x0, -1;  \
  addi  x17, x0, -8;  \
  addi  x18, x0, -15; \
  addi  x19, x0, -22; \
  addi  x20, x0, -29; \
  addi  x21, x0, -36; \
  addi  x22, x0, -43; \
  addi  x23, x0, -50; \
  addi  x24, x0, -57; \
  addi  x25, x0, -64; \
  addi  x26, x0, -71; \
  addi  x27, x0, -78; \
  addi  x28, x0, -85; \
  addi  x29, x0, -92; \
  addi  x30, x0, -99; \
  addi  x31, x0, -106;

// *****************************************************************************
#define BASIC_TEST_CASE_BRANCH(inst, rs1, rs2)\
  inst rs1, rs2, test_fail;

#define BASIC_TEST_CASE_ADDI(result)\
  START_TEST              \
  addi  x29, x0, result;  \
  bne   x29, x31, test_fail;

// *****************************************************************************
#endif  //TEST_UTILS_H
