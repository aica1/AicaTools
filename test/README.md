# Programa Teste

Códigos em ASM para testar o funcionamento do processador, sendo dividido em tipos: instr (instrução), core (núcleo), dcache (cache de dados) e io (disp. I/O).

- **instr**: testa a execução das instruções, considerando que todo o programa está presente na cache, pois o objetivo é testar unicamente sua execução dentro do pipeline;
- **core**:	testa situações específicas do núcleo, como o uso do forward, preditor de desvios, load-stall, entre outros casos, contendo novamente todo o programa já na cache (muitos dos casos aqui ocorrem de uma forma ou outra em **instr**);
- **dcache**: realiza operação de SW/SH/SB para verificar o funcionamento correto da cache, forçando a ocorrência de miss, e em certos casos as operações de write-back;
- **io**: verifica o funcionamento dos periféricos do núcleo, UART e temporizador.

O primeiro tipo de teste a ser executado deve ser o **instr**, seguido pelo **core**, para garantir o funcionamento do núcleo. A sequência **dcache** e **io** pode ser alternada. Os testes ao falharem carregam a string "ERRO" nos registradores x31-x28, armazenando o número do teste que falhou (definido nas macros, dentro dos códigos ASM) no registrador x24. Em uma execução correta do teste, carrega-se a string "OK" nos registradores x31-x30.

## Instruções
O programa de teste foi dividido entre diversos arquivos, a fim de se atenuar a cadeia de dependências entre as instruções que devem ser testadas. Exemplo: **__ADDI__** quando utilizado com a constate 0 (registrador x0) se torna a pseudo-instrução **__Load Immediate__**, utilizada com frequência para inicializar os registradores com um valor X de teste.

1. **basic**: contém a execução das instruções __BNE__, __BEQ__, __ADDI__ e __LUI__, que são essênciais nos demais teste;
2. **branch**: realiza testes de desvios condicionais, por meio das instruções __BNE__, __BEQ__, __BGE[U]__ e __BLT[U]__;
3. **reg2reg**: realiza operações lógicas e aritméticas entre dois registradores, conténdo as instruções __ADD__, __SUB__, __OR__, __AND__, __XOR__, __SLT[U]__, __SLL__, __SRL__ e __SRA__;
4. **r2imm**: realiza operações lógicas e aritméticas entre um registrador e o sinal imediato, sendo dependente apenas do resultado da instruções __SUB__ do teste 3. Contém as instruções __ADDI__, __ORI__, __ANDI__, __XORI__, __SLTI[U]__, __SLLI__, __SRLI__ e __SRAI__;
5. **jump**: testa as operações de desvios incondicionais, pelas instruções __JAL__ e __JALR__;
6. **load**: realiza acessos de busca de dados na memória, por meio das instruções __LW__, __LH[U]__ e __LB[U]__, não sendo dependente do teste 5;
7. **store**: sendo o último teste do núcleo, utiliza as operações de load para verificar o armazenamento correto de dados por meio das instruções __SW__, __SH__ e __SB__.

## TODO

- teste de exceção
- teste de interrupção
